Overview
-------
This project was developed as a response to the large number of beginning Python developer who want to learn how to write a text adventure.

Although there are many approaches, this project is designed to be expandable with "pluggable" elements.

Tutorial
--------
For an in-depth tutorial, please see [How to Write a Text Adventure in Python](http://letstalkdata.com/2014/08/how-to-write-a-text-adventure-in-python/).

Differences between this and master.
--------
There are a few differences between this version and the master version:  
1) All base-classes with it's own \_\_str\_\_() function that doesn't radically change things uses the super().\_\_init\_\_ in addition to it's changes.
2) The inclusion of a shield type.
3) Two different forms of 'rooms'. The old tile-based mechanism and the new room-based mechanism. 

Differences between tiles and rooms.
--------
Tiles - The tile based mechanism uses an external file and setup to load the world. This requires tabs, and further it's layout is completely dependent upon an external file and the world's layout could be borked by a change that would cause a room to be placed in the wrong location. Further this requires a one-room seperation to prevent entrances to other rooms. Preventing layouts where there are two rooms next to each other, but no connection between them. This is fine for the cave layout, but not so fine for dungeons and overworld stuff. However it should be noted that the tile approach is simpler than the Room approach. 

Rooms - The room based mechanism uses it's own loading function, in which rooms are programmatically formed. Each room defines it's own entrances and exits and link to another room. Each room has it's own setup and can have unique properties. Further Rooms allow two areas to be placed next to each-other on the map, but have no connection and also has it to where Rooms can connect to another room that would be logically across the map without having to create an entirely new type of room for that. It should be noted that this is slightly more complicated than the Room approach due to the fact that this is dependent upon programmatically reading and creating the rooms. I plan to make a parser with a declarative language to setup rooms in the future though, which should simplify it a bit more and bring it near on-par with rooms.  

 
