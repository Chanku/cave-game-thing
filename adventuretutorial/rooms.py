import items, enemies, actions
#Describes all of the rooms in the world space.
__author__ = 'Sapein/Chanku'

class BaseRoom():
    #The base class for all rooms within the world space.
    def __init__(self, room_id, nexit, sexit, eexit, wexit, inventory, enemies, respawn_allowed=False):
        #Creates a new room
        #room_id: The ID of the room used when loading it up.
        #_exit: Sets the exit of the room, with a room ID. The starting letter defines the directional exit (n,s,e,w = North, South, East, and West respectively)
        #inventory: The room's inventory. It contains items that can be found within the room.
        #enemies: The room's enemies (if any). It contains the enemies that can be found within the room.
        #respawn_allowed: This will check and see if enemy respawning is allowed within the room
        self.room_id = room_id
        self.nexit = nexit
        self.sexit = sexit
        self.eexit = eexit
        self.wexit = wexit
        self.inventory = inventory
        self.enemies = enemies
        self.respawn_allowed = respawn_allowed

    def intro_text(self):
        #Information to be displayed when the player moves into the tile.
        raise NotImplementedError()

    def adjacent_moves(self):
        #Returns all available moves for the room.
        moves = []
        if self.nexit != None:
            moves.append(actions.MoveNorth())
        if self.sexit != None:
            moves.append(actions.MoveSouth())
        if self.eexit != None:
            moves.append(actions.MoveEast())
        if self.wexit != None:
            moves.append(actions.MoveWest())
        return moves

    def check_enemies(self):
        #This checks to see if enemies should be removed
        if self.enemies:
            for item in self.enemies:
                if not item:
                    pass
                elif not item.is_alive():
                    self.enemies.remove(item)

    def available_actions(self):
        #Returns all available actions in the room.
        self.check_enemies()
        moves = self.adjacent_moves()
        moves.append(actions.ViewInventory())
        moves.append(actions.EquipItem())
        moves.append(actions.RemoveItem())
        for item in self.inventory:
            if isinstance(item, items.Item):
                moves.append(actions.PickUpItem())
                break
        for mob in self.enemies:
            if isinstance(mob, enemies.Enemy):
                moves.append(actions.Attack())
                moves.append(actions.Defend())
                moves.append(actions.Magic())
                moves.append(actions.Flee())
                break
        return moves

    def spawn(self):
        #Spawn: Returns None if nothing can (re)spawn 
        #Spawn: Returns False if only monsters can (re)spawn
        #Spawn: Returns True if either players can (re)spawn here.
        #Spawn: Can return "all" if both monsters and players can (re)spawn here.
        raise NotImplementedError

class BossRoom(BaseRoom):
    def __init__(self, room_id, nexit, sexit, eexit, wexit, inventory, enemies, respawn_allowed=False):
        super().__init__(room_id=room_id, nexit=nexit, sexit=sexit, eexit=exit, wexit=wexit, inventory=inventory, enemies=enemies, respawn_allowed=respawn_allowed)

    def spawn(self):
        raise NotImplementedError

    def intro_text(self):
        raise NotImplementedError
