#A simple text adventure based off of Phillip Johnson's game for new programmers. 
__author__ = 'Chanku/Sapein'
__author2__ = 'Phillip Johnson'
import world 
from rooms import BossRoom
from player import Player


def play_2():
    world.load_worlds()
    player = Player()
    print(world._world[0].intro_text())
    room = player.room
    while player.is_alive and not player.victory:
        room = world._world[player.room]
        print("Choose an action:")
        available_actions = room.available_actions()
        for action in available_actions:
            print(action)
        action_input = input("Action: ")
        for action in available_actions:
            if action_input == action.hotkey:
                player.do_action(action, **action.kwargs)
                break
        if isinstance(room, BossRoom):
            x = False
            for item in room.enemies:
                if item.name == "Yugath the Old":
                    x = True
            if not x:
                print("You have won!")
                player.victory = True

def play():
    world.load_tiles()
    player = Player()
    room = world.tile_exists(player.location_x, player.location_y)
    print(room.intro_text())
    while player.is_alive() and not player.victory:
        room = world.tile_exists(player.location_x, player.location_y)
        room.modify_player(player)
        # Check again since the room could have changed the player's state
        if player.is_alive() and not player.victory:
            print("Choose an action:\n")
            available_actions = room.available_actions()
            for action in available_actions:
                print(action)
            action_input = input('Action: ')
            for action in available_actions:
                if action_input == action.hotkey:
                    player.do_action(action, **action.kwargs)
                    break


if __name__ == "__main__":
    play_2()
