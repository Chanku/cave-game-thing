#Describes the items in the game.
__author__ = 'Phillip Johnson'
__author2__ = 'Sapein/Chanku'

class Item():
    #The ultimate base class for all items in the game
    def __init__(self, name, description, value):
        self.name = name
        self.description = description
        self.value = value

    def __str__(self):
        return "{}\n=====\n{}\nValue: {}\n".format(self.name, self.description, self.value)

class Shield(Item):
    def __init__(self, name, description, value, defense):
        self.defense = defense
        super().__init__(name, description, value)
    
    def __str__(self):
        return super().__str__() + "Defense: {}".format(self.defense)

class Weapon(Item):
    def __init__(self, name, description, value, damage):
        self.damage = damage
        super().__init__(name, description, value)

    def __str__(self):
        return super().__str__() + "Damage: {}".format(self.damage)

class Fist(Weapon):
    def __init__(self):
        super().__init__(name="Fist", description="Your bare fists", value=0, damage=4)

    def __str__(self):
        return ""

class Rock(Weapon):
    def __init__(self):
        super().__init__(name="Rock", description="A fist-sized rock, suitable for bludgeoning.", value=0, damage=5)


class Dagger(Weapon):
    def __init__(self):
        super().__init__(name="Dagger", description="A small dagger with some rust. Somewhat more dangerous than a rock.", value=10, damage=10)

class HylianShield(Shield):
    def __init__(self):
        super().__init__(name="Hylian Shield", description="A shield from Hyrule!", value=10, defense=10)
class Gold(Item):
    def __init__(self, amt):
        self.amt = amt
        super().__init__(name="Gold", description="A round coin with {} stamped on the front.".format(str(self.amt)), value=self.amt)
