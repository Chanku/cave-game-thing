"""Defines the enemies in the game"""
__author__ = 'Phillip Johnson' 

class Enemy:
    """A base class for all enemies"""
    def __init__(self, name, hp, damage, defense):
        """Creates a new enemy

        :param name: the name of the enemy
        :param hp: the hit points of the enemy
        :param damage: the damage the enemy does with each attack
        :param defense: the damage the enemy blocks with each player's attack
        """
        self.name = name
        self.hp = hp
        self.damage = damage
        self.defense = defense

    def is_alive(self):
        return self.hp > 0


class GiantSpider(Enemy):
    def __init__(self):
        super().__init__(name="Giant Spider", hp=10, damage=2, defense=0)


class Ogre(Enemy):
    def __init__(self):
        super().__init__(name="Ogre", hp=30, damage=15, defense=10)

class Boss(Enemy):
    def __init__(self):
        super().__init__(name='Yugath the Old', hp=35, damage=20, defense=15)
