import random, items, world
__author__ = 'Phillip Johnson'
__author2__ = "Sapein/Chanku"

class Player():
    def __init__(self):
        self.inventory = [items.Gold(15)]
        self.equipped = [items.Fist()]
        self.hp = 100
        # self.location_x, self.location_y = world.starting_position
        self.room = 0
        self.victory = False

    def is_alive(self):
        return self.hp > 0

    def do_action(self, action, **kwargs):
        action_method = getattr(self, action.method.__name__)
        if action_method:
            action_method(**kwargs)

    def print_inventory(self):
        for item in self.inventory:
            print(item, '\n')

    def move(self, room):
        print(world._world[room].intro_text())

    def move_north(self):
        for item in world._world:
            if self.room == item.room_id:
                if item.nexit or item.nexit == 0:
                    self.room = item.nexit
                    self.move(self.room)
                    break

    def move_south(self):
        for item in world._world:
            if self.room == item.room_id:
                if item.sexit or item.sexit == 0:
                    self.room = item.sexit
                    self.move(self.room)
                    break

    def move_east(self):
        for item in world._world:
            if self.room == item.room_id:
                if item.eexit or item.eexit == 0:
                    self.room = item.eexit
                    self.move(self.room)
                    break

    def move_west(self):
        for item in world._world:
            if self.room == item.room_id:
                if item.wexit or item.wexit == 0:
                    self.room = item.wexit
                    self.move(self.room)
                    break

    def attack(self):
        r = world._world[self.room]
        iterator = 0
        for monster in r.enemies:
            print("{}: {} || HP: {} ; Atk: {} ; Def: {}".format(str(iterator), monster.name, monster.hp, monster.damage, 
                monster.defense))
            iterator += 1
        sel_mob = input("Select a mob to attack: ")
        if sel_mob == '':
            print("No mob selected!")
            return 0
        sel_mob = r.enemies[int(sel_mob)]
        for i in self.equipped:
            if isinstance(i, items.Weapon):
                print("You use {} against {}!".format(i.name, sel_mob.name)) 
                damage = i.damage - sel_mob.defense
                if damage < 0:
                    damage = 0
                sel_mob.hp -= damage
        if not sel_mob.is_alive():
            print("You killed {}!".format(sel_mob.name))
        else:
            print("{} HP is {}.".format(sel_mob.name, sel_mob.hp))
            damage = 0
            for mob in r.enemies:
                damage += mob.damage - 1 
                if damage < 0:
                    damage = 0
            self.hp -= damage
        print("Your HP is {}".format(self.hp))

    def defense(self):
        #Allows you to defend against an attack.
        r = world._world[self.room]
        iterator = 0
        for monster in r.enemies:
            print("{}: {} || HP: {} ; Atk: {} ; Def: {}".format(str(iterator), monster.name, monster.hp, monster.damage, 
                monster.defense))
            iterator += 1
        sel_mob = input("Select a mob to defend against: ")
        if sel_mob == '':
            print("No mob selected!")
            return 0
        sel_mob = r.enemies[int(sel_mob)]
        x = False
        for i in self.equipped:
            if isinstance(i, items.Shield):
                print("You defend against {} with {}!".format(sel_mob.name, i.name))
                x = True
                damage = 0
                for mob in r.enemies:
                    damage += mob.damage - i.defense
                    if damage < 0:
                        damage = 0
                self.hp -= damage 
        if not x:
            damage = 0
            for mob in r.enemies:
                damage += mob.damage - 1 
                if damage < 0:
                    damage = 0
            self.hp -= damage 
        if not sel_mob.is_alive():
            print("You killed {}!".format(sel_mob.name))
        else:
            print("{} HP is {}.".format(sel_mob.name, sel_mob.hp))
        print("Your HP is {}".format(self.hp))

    def pick_up(self):
        #Picks up an item in a room.
        r = world._world[self.room]
        i = 0
        for item in r.inventory:
            print("{} : {}".format(str(i), item.name))
            i += 1
        item = input("Please enter the number of the item you wish to pickup: ")
        item = int(item)
        if isinstance(r.inventory[item], items.Item):
            print("Item {} picked up!".format(r.inventory[item].name))
            self.inventory.append(r.inventory[item])
            item = r.inventory[item]
            r.inventory.remove(item)

        
    def equip(self):
        #Equips an item
        i = 0
        for item in self.inventory:
            print("{}: {}".format(str(i), item.name))
            i += 1
        sel_item = input("Select the item you wish to equip: ")
        if sel_item == '':
            print("No item selected!")
            return 0
        sel_item = self.inventory[int(sel_item)]
        x = False
        if isinstance(sel_item, items.Weapon) or isinstance(sel_item, items.Shield):
            for item in self.equipped:
                if item == sel_item:
                    print("{} is already equipped!".format(sel_item.name))
                    x = True
                    break
            if not x:
                for i in self.equipped:
                    if isinstance(i, items.Weapon):
                        self.equipped.remove(i)
                    elif isinstance(i, items.Shield):
                        self.equipped.remove(i)
                    self.equipped.append(sel_item)
        else:
            print("{} is not equippable!".format(sel_item.name))

    def remove(self):
        #Removes an item
        i = 0
        for item in self.equipped:
            if item.name.lower() != 'fist':
                print("{}: {}".format(str(i), item.name))
                i += 1
        sel_item = input("Select the item you wish to remove: ")
        if sel_item == '':
            print("No item selected!")
            return 0
        sel_item = self.equipped[int(sel_item)]
        self.equipped.remove(sel_item)
        if isinstance(sel_item, items.Weapon):
            self.equipped.append(items.Fist())
        print("{} has been successfully removed!".format(sel_item.name))
