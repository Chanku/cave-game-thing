import items, enemies, game 
from rooms import BaseRoom
_author_ = 'Chanku/Sapein'
_world = [] 

def load_worlds():
    _world.append(StartRoom())
    _world.append(TestRoom())
    _world.append(TestRoom_Inv())
    _world.append(TestRoom_M1())

class StartRoom(BaseRoom):
    def __init__(self):
        super().__init__(room_id=0, nexit=1, sexit=2, eexit=None, wexit=None, inventory=[None], enemies={None})

    def spawn(self):
        return True

    def intro_text(self):
        return "You awaken in a room. You look around, dazzed and confused. You notice an exit to the north"
    
    def modify_player(self):
        pass

class TestRoom(BaseRoom):
    def __init__(self):
        super().__init__(room_id=1, nexit=None, sexit=0, eexit=None, wexit=None, inventory=[None], enemies={None})

    def spawn(self):
        pass

    def intro_text(self):
        return "You walk into this room and it says that you have entered the test room...it says to go fuck yourself"
    
    def modify_player(self):
        pass

class TestRoom_Inv(BaseRoom):
    def __init__(self):
        super().__init__(room_id=2, nexit=0 , sexit=3, eexit=None, wexit=None, inventory=[items.Dagger(), items.HylianShield()], enemies=[None])

    def spawn(self):
        pass

    def intro_text(self):
        return "You walk into a room, it has pictures of a cat, below it there is a plaque that reads: Schildkroetenhund.There is something recessed in the wall..."

    def modify_player(self):
        pass

class TestRoom_M1(BaseRoom):
    def __init__(self):
        super().__init__(room_id=3, nexit=2, sexit=None, eexit=None, wexit=None, inventory=[None], enemies=[enemies.GiantSpider()])
    def spawn(self):
        pass

    def intro_text(self):
        return "There is a giant spider in this room"
    
    def modify_player(self):
        pass

