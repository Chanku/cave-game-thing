import items, enemies, game
from rooms import BaseRoom, BossRoom
_author_ = 'Chanku/Sapein'
_world = []

def load_worlds():
    _world.append(StartRoom())
    _world.append(DungeonStart())
    _world.append(TurtleRoom())
    _world.append(EmptyRoom())
    _world.append(ShieldRoom())
    _world.append(DaggerRoom())
    _world.append(SpiderRoom())
    _world.append(EnterRoom())
    _world.append(OgreRoom())
    _world.append(YugathRoom())

class StartRoom(BaseRoom):
    def __init__(self):
        super().__init__(room_id=0, nexit=1, sexit=None, eexit=None, wexit=None, inventory=[None], enemies=[None])

    def spawn(self):
        return True

    def intro_text(self):
        return "You are standing at the entrance of a dungeon. You can not turn back..."

    def modify_player(self):
        pass

class DungeonStart(BaseRoom):
    def __init__(self):
        super().__init__(room_id=1, nexit=None, sexit=None, eexit=3, wexit=2, inventory=[None], enemies=[None])

    def spawn(self):
        pass

    def intro_text(self):
        return "You enter the dungeon. There is a door to the east and a door to the west..." 

    def modify_player(self):
        pass

class TurtleRoom(BaseRoom):
    def __init__(self):
        super().__init__(room_id=2, nexit=4, sexit=None, eexit=1, wexit=None, inventory=[None], enemies=[None])

    def spawn(self):
        pass

    def intro_text(self):
        return "You walk into the room and a turtle-dog thing appears. It tells you to head to the north room and beware Yugath the Old, it then disappears."

    def modify_player(self):
        pass

class EmptyRoom(BaseRoom):
    def __init__(self):
        super().__init__(room_id=3, nexit=6, sexit=None, eexit=0, wexit=1, inventory=[None], enemies=[None])

    def spawn(self):
        pass

    def intro_text(self):
        return "The room is empty except for an exit to the north and to the south..."

    def modify_player(self):
        pass

class ShieldRoom(BaseRoom):
    def __init__(self):
        super().__init__(room_id=4, nexit=None, sexit=2, eexit=5, wexit=None, inventory=[items.HylianShield()], enemies=[None])
    
    def spawn(self):
        pass

    def intro_text(self):
        return "There is a shield from another world...the plaque says it is a Hylian Shield from Hyrule..."

    def modify_player(self):
        pass

class DaggerRoom(BaseRoom):
    def __init__(self):
        super().__init__(room_id=5, nexit=None, sexit=None, eexit=6, wexit=4, inventory=[items.Dagger()], enemies=[None])

    def spawn(self):
        pass

    def intro_text(self):
        return "There is a dagger in the center of the room...it may be cursed, but it beats your fists..."

    def modify_player(self):
        pass

class SpiderRoom(BaseRoom):
    def __init__(self):
        super().__init__(room_id=6, nexit=7, sexit=3, eexit=None, wexit=5, inventory=[None], enemies=[enemies.GiantSpider()])
    
    def spawn(self):
        pass

    def intro_text(self):
        return "There is a giant spider in the room. You can attack it...but should you?"

    def modify_player(self):
        pass

class EnterRoom(BaseRoom):
    def __init__(self):
        super().__init__(room_id=7, nexit=8, sexit=6, eexit=None, wexit=None, inventory=[None], enemies=[None])

    def spawn(self):
        pass

    def intro_text(self):
        return "You are alone in the room. You can hear an Ogre in the next room...do you dare continue?"

    def modify_player(self):
        pass

class OgreRoom(BaseRoom):
    def __init__(self):
        super().__init__(room_id=8, nexit=None, sexit=7, eexit=None, wexit=9, inventory=[None], enemies=[enemies.Ogre()])

    def spawn(self):
        pass

    def intro_text(self):
        return "You enter the room, there is an Ogre in here, although it is chained. If you attack it you will get hurt...but it might be worth it..."

    def modify_player(self):
        pass

class YugathRoom(BossRoom):
    def __init__(self):
        super().__init__(room_id=9, nexit=None, sexit=None, eexit=None, wexit=None, inventory=[None], enemies=[enemies.Boss()])

    def spawn(self):
        pass

    def intro_text(self):
        return "You enter the seemingly empty room and the exit shuts behind you trapping you. A being appears in the room and it speaks loudly, \"I am Yugath the Old. Many have come, all have failed. You shall die.\" It looks like you must fight as there is no exit."
